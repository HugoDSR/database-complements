/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Hugo Rocha 68511
 * @author João Ribeiro 64649
 * @author Andre Carvalho 64257
 *
 */
public class Modificar extends InterfaceConnection {

    /**
     * Creates new form Modificar
     */
    public Menu2 menu2;
    private DBCollection thisTable;
    
    public Modificar() throws UnknownHostException{
        if(Menu1.id == 1){
            thisTable = db.getCollection("aluno");
        }
        else if (Menu1.id == 2){
            thisTable = db.getCollection("professor");
        }
        else if (Menu1.id == 3){
            thisTable = db.getCollection("funcionario");
        }
        initComponents();
    }

    //funçao para listar no panel
    private void listPanel(DBCursor cursor, BasicDBObject query) {
        cursor = thisTable.find(query);

        //se nao existirem resultados nesta pesquisa
        if (!cursor.hasNext()) {
            jTextArea1.setText("Nao existem resultados!");
        }
        while (cursor.hasNext()) {
            jTextArea1.setText(jTextArea1.getText() + "\n" + cursor.next());
        }
    }

    //verificar se textbox contem conteudo de pesquisa
    private void emptyValid() {
        if ("".equals(jTextField1.getText())) {
            JOptionPane.showMessageDialog(null, "Insira o que pretende pesquisar!");
        }
    }
    
    private boolean queryExists(DBCursor cursor, BasicDBObject query){
        cursor = thisTable.find(query);

        //DBObject query = new BasicDBObject(field, new BasicDBObject( "$ne", "").append("$exists", true)).append("_id", key);
        //return t.count(query) == 1;
        if (cursor.hasNext()){
            return true;
        }
        else{
            return false;
        }

    }
    
    private void updateField(int p, String op, String field, String newValue){

        DBCursor cursor = null;
        
        switch(p){
            case 1:
                /**** Update ****/
                // search document where name=op and update it with new values
                BasicDBObject query = new BasicDBObject();
                query.put("name", op);

                BasicDBObject newDocument = new BasicDBObject();
                newDocument.put(field, newValue);

                BasicDBObject updateObj = new BasicDBObject();
                updateObj.put("$set", newDocument);

                BasicDBObject queryValida = new BasicDBObject();
                queryValida.put(field, newValue); 
                
                if (!queryExists(cursor, queryValida)){
                    thisTable.findAndModify(query, updateObj);
                    //table.update(query, updateObj)        //Só assim: não apaga valor anterior, apenas cria um novo
                    //table.remove(query);                  
                    JOptionPane.showMessageDialog(null, "Updated\n "+thisTable.find(queryValida).next());
                }else{
                    JOptionPane.showMessageDialog(null, "Parcela (campo - valor) já existente!");
                }

                break;
                
            case 2:
                /**** Update ****/
                // search document where name=op and update it with new values
                BasicDBObject query2 = new BasicDBObject();
                query2.put("mec", op);

                BasicDBObject newDocument2 = new BasicDBObject();
                newDocument2.put(field, newValue);

                BasicDBObject updateObj2 = new BasicDBObject();
                updateObj2.put("$set", newDocument2);

                BasicDBObject queryValida2 = new BasicDBObject();
                queryValida2.put(field, newValue); 
                
                if (!queryExists(cursor, queryValida2)){
                    thisTable.findAndModify(query2, updateObj2);
                    //table.update(query2, updateObj2)           //Só assim, não apaga valor anterior, apenas cria um novo         
                    //table.remove(query2);                    
                    JOptionPane.showMessageDialog(null, "Updated\n "+thisTable.find(queryValida2).next());
                }else{
                    JOptionPane.showMessageDialog(null, "Parcela (campo - valor) já existente!");
                }
                break;
                
            case 3:
                /**** Update ****/
                // search document where name=op and update it with new values
                BasicDBObject query3 = new BasicDBObject();
                query3.put("nif", op);

                BasicDBObject newDocument3 = new BasicDBObject();
                newDocument3.put(field, newValue);

                BasicDBObject updateObj3 = new BasicDBObject();
                updateObj3.put("$set", newDocument3);

                BasicDBObject queryValida3 = new BasicDBObject();
                queryValida3.put(field, newValue); 
                
                if (!queryExists(cursor, queryValida3)){
                    thisTable.findAndModify(query3, updateObj3);
                    //table.update(query3, updateObj3)          //Só assim, não apaga valor anterior, apenas cria um novo
                    //table.remove(query3);     
                    JOptionPane.showMessageDialog(null, "Updated\n "+thisTable.find(queryValida3).next());

                }else{
                    JOptionPane.showMessageDialog(null, "Parcela (campo - valor) já existente!");
                }
                break;
                
            default:
                JOptionPane.showMessageDialog(null, "Erro ao fazer Update");
                break;
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jButton3 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton2.setText("Pesquisar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jRadioButton3.setText("NIF");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        jRadioButton2.setText("Nº Mecanográfico");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jRadioButton1.setText("Nome");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon("/home/joao/Documentos/CBD/MongoSwing/src/main/java/images/Back_icon.png")); // NOI18N
        jButton3.setText("Retroceder");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jLabel1.setText("Campo");

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jLabel2.setText("Valor");

        jButton1.setText("Alterar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addContainerGap(724, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 694, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jRadioButton1)
                            .addGap(127, 127, 127)
                            .addComponent(jRadioButton2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jRadioButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(135, 135, 135))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jButton2)
                            .addGap(288, 288, 288)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField3)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)))
                .addGap(83, 83, 83))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton3)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jRadioButton3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1))
                .addContainerGap(102, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(910, 630));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        //para pesquisas, criaçao de query e cursor
        DBCursor cursor = null;
        BasicDBObject query = new BasicDBObject();

        //clear do log na segunda iteração
        jTextArea1.setText("");

        if (jRadioButton1.isSelected() && !jRadioButton2.isSelected() && !jRadioButton3.isSelected()) { //Pesquisar por nome
            emptyValid();
            query.put("name", jTextField1.getText().toUpperCase());
            //funcao criada para listar o panel
            listPanel(cursor, query);

        } else if (jRadioButton2.isSelected() && !jRadioButton1.isSelected() && !jRadioButton3.isSelected()) { //Pesquisar por mec
            emptyValid();
            query.put("mec", jTextField1.getText());
            listPanel(cursor, query);

        } else if (jRadioButton3.isSelected() && !jRadioButton2.isSelected() && !jRadioButton1.isSelected()) { //pesquisar por nif
            emptyValid();
            query.put("nif", jTextField1.getText());
            listPanel(cursor, query);

        } else {
            JOptionPane.showMessageDialog(null, "Selecione uma das opções!");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        jRadioButton2.setSelected(false);
        jRadioButton3.setSelected(false);
        
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            menu2 = new Menu2();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Modificar.class.getName()).log(Level.SEVERE, null, ex);
        }
        menu2.setVisible(true);
        setVisible(false);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int pos = 0;
        BasicDBObject query = new BasicDBObject();

        if (jRadioButton1.isSelected()) { //Pesquisar por nome
            //query.put("name", jTextField1.getText());
            pos = 1;
        } else if (jRadioButton2.isSelected() ) { //Pesquisar por mec
            //query.put("mec", jTextField1.getText());
            pos = 2;
        } else if (jRadioButton3.isSelected() ) { //pesquisar por nif
            //query.put("nif", jTextField1.getText());
            pos = 3;
        }
        
        // Validar campo nao vazio e existente
        if (jTextField2.getText().equals("") || jTextField3.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Campo e/ou Valor vazios");
            jTextField2.requestFocusInWindow();
        
        //} else if(queryExists(table, jTextField2.getText(), jTextField3.getText())){
        //    JOptionPane.showMessageDialog(null, "Existe Linha (FIELD EXISTS MAL)");
        } else{
            updateField(pos, jTextField1.getText(), jTextField2.getText().toUpperCase(), jTextField3.getText().toLowerCase());
        }
        
        // Update BD
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        jRadioButton1.setSelected(false);
        jRadioButton3.setSelected(false);
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        jRadioButton1.setSelected(false);
        jRadioButton2.setSelected(false);
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Modificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Modificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Modificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Modificar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new Modificar().setVisible(true);
                } catch (UnknownHostException ex) {
                    Logger.getLogger(Modificar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    // End of variables declaration//GEN-END:variables
}
